# OpenML dataset: Bioresponse

https://www.openml.org/d/45017

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "classification on numerical features" benchmark. 
 
  Original link: https://openml.org/d/4134 
 
 Original description: 
 
**Author**: Boehringer Ingelheim  
**Source**: [Kaggle](https://www.kaggle.com/c/bioresponse) - 2011  
**Please cite**: None  

Predict a biological response of molecules from their chemical properties. Each row in this data set represents a molecule. The first column contains experimental data describing an actual biological response; the molecule was seen to elicit this response (1), or not (0). The remaining columns represent molecular descriptors (d1 through d1776), these are calculated properties that can capture some of the characteristics of the molecule - for example size, shape, or elemental constitution. The descriptor matrix has been normalized.

The original training and test set were merged.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45017) of an [OpenML dataset](https://www.openml.org/d/45017). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45017/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45017/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45017/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

